## Algorithms

\textbf{Apriori} - Used for looking for relation in champion compositions and winning.
\textbf{GSP algorithm Frequent pattern mining}  - used for the determining if a given sequence of events leads to victory
\textbf{ID3} - Classification algorithm which predicts the outcome class based on generated model.
