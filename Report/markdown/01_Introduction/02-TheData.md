﻿## The Data

All of our data comes from the game's own API called RiotAPI from Riot Games and did not need any kind of cleaning. We choose only to do our examinations/mining on a limited amount of matches, 1000 matches to be exact. It is possible to expand and mine matches if wanted. We used a library called Orianna which is an API that utilises RiotAPI and provides some functionality we would otherwise not have. In combination with Orianna we stored most of our data on memory and physical files for quicker and offline availability. We have a loader that loads needed data and relevant associated data, such as champion names. Then each algorithm can transform the data to fit its use since the three algorithms works with different parts of the data.  

As said above  we want to use data from matches played in a game mode called Ranked-Solo-Queue, since it is the most played game mode. One player is matched via matchmaking with other players of the same level or in same level range as other joining players. 

The data consist of a variety of information for a match. For example: what champions was attending the match, what items did they buy, what team won. and lots more.
We focused on following information: team compositions, the winning team for a match, tower kills, dragon kills, baron kills and timelines for the match.
