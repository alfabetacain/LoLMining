## Frequent pattern - Generalized Sequential Pattern algorithm (GSP)

Since we had access to not only statistics of a given match, but also the timeline and the events that occurred during the match, we decided to try to use the GSP algorithm. The purpose was to determine if a given sequence of events would inevitably lead to victory for either team one or team two. 
The structure of the data matched the structure that the GSP algorithm expected. Events were ordered in “frames”, which basically is a set of events and a timestamp. This was analogous to “transactions” in the GSP algorithm.

## Data preprocessing

First, the timelines did not include an event for the game ending, but since we could find this data in the match class, we created our own custom event type and appended it to each match’s timeline, given it the timestamp of the match duration. Furthermore, in order to hash each event to our chosen degree of uniquity, each event was wrapped in a container type, which provided a custom hashing method.
After the early results it became apparent that it would be beneficial to make some attributes less unique. This was due to the overwhelming numbers of some event types, primarily item events and ward[^1] events, which in addition to obscuring less often, but more interesting patterns, also hurt the performance of the GSP algorithm. Because of that, both wards and items were generalised to be on a team level and wards were generalised to not distinguish between different ward types.

<!-- Footnotes: -->
[^1]: A ward is an item which makes the area around it visible when it is placed. It expires after some time.

## Results:

The algorithm was run with a window size of 1 ms, max gap of 20 seconds, minimum support of 200 and confidence of 0.9. The reason for the minimum support to only be a fifth of the data is that we found that if we set it too high, the results would be trivial, and thus we hoped that by setting it to a fifth we might be able to capture some of the more nontrivial results. 

The results in full can be seen in the appendix, in the file named gspAssociations.txt. 

One interesting result is the association 
“CHAMPION_KILL TEAM ONE” -> “CHAMPION_KILL TEAM TWO” with the confidence of 0.99. From this we gather that it is very rare for one team to get no kills during a match. While this information might be trivial, the rarity of this event seems interesting. It could indicate that almost no match is entirely one-sided. It should also be noted that since max gap is 20 seconds, these events takes place shortly after one another. 

Another interesting association is 
“BUILDING_KILL TOWER_BUILDING TEAM ONE” -> “CHAMPION_KILL TEAM TWO” with confidence 0.97. 
It would seem that if team one manages to destroy a tower, it is very likely that within 20 seconds one on the opposing team will die. This could very well be because it is risky to take an enemy tower and that it is very likely that one on your team will die trying. 
It could suggest that before tower kills, team fights often occurs. 
It could also just be because champion kills occurs very often in our data. 

In general the number of associations generated is very high, even with a confidence of 0.9. This is mainly because of the lower minimum support we used and thus was to be expected. Wards and item purchases fill up our data and the main conclusion to draw from that is that they both occur very often. Because of this fact, it is likely that most associations including these do not as much suggest a link between them as suggest that ward placements and item purchases takes place all the time. 

It should also be noted that there are not a single association regarding how the game ended. This seems to indicate that there are no definitive link between the events in a game and how it ended.

Unfortunately our results from the application of the GSP algorithm did not return any definite non trivial associations and thus were not applicable for use in our classification.



