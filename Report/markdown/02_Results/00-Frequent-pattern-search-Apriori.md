# Frequent pattern search - Apriori

The Apriori algorithm works by taking transactions, where each transaction is seen as a set of items (an itemset). Given a threshold , the Apriori algorithm identifies the item sets which are subsets of at least  transactions in our dataset.

## Results

Our algorithm didn’t give us anything conclusive since we only had items with a support of around 50 out of 1000 - 138 out of 1000 and a confidence of about 0.5. With this kind of result you could argue that the game may actually be extremely well balanced. This might be the result of frequent updates to the champions currently in the game, because Riot Games are interested in balancing the game as much as possible. 
In this regard it looks like they have succeeded. When Riot Games release a new champion it comes with some stats which determine the strengths of the champion. During a season people try this champion and the champions stats might prove to be overpowered, so that the champion is much stronger than every other champion available for players. Riot Games will then on request from the players adjust the stats to a more fitting level. 
During 2012 Riot Games released a champion that was very unbalanced and no one was able to best it. Players complained to Riot Games to reduce the power of the champion. In the data we have, no new champions have been added this season and this might have changed our results.
In our case we have no champion that stands out. The only one that comes to mind is this:

[Jinx](342) -> [WINNER](177)=0.5175438596491229

As one can see the support (numbers in parentheses) and confidence is very low. A confidence of 0.52 is telling that in 52% of the times this statistically happens.
All of these results are from where a player can pick the champion he/she wants to play but our results might have changed a great deal if this wasn’t the case. Putting the player in different situations with a champion that player haven’t played before can change the course of the match and therefore the results of our data.

For the combination [champion, champion, champion, champion,champion, game result],
our intention was to see if a specific combination of champions ended in a specific end result (Win/Loss). With this we could then have determined if this combination of heroes lose more frequently than other compositions. This was not the case though as mentioned above.
.
See the appendix in the file singleTeamApriori.txt for the apriori results based on a single team.

For the combination [Team, opposing Team, win] our intention was to see if a champion composition won against a specific opposing team composition.
This showed us almost the same picture as the above combination.
if any interesting patterns like [Champion, opposing Team, win] were present, they would show as subset of the set [Team, opposing Team, win].
The results for the apriori performed on both teams are in the appendix, in the file doubleTeamApriori.txt.
