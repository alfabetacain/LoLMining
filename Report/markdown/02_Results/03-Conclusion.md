## Conclusion

All in all it was unfortunate that neither the Apriori nor the GSP algorithm produced results useful for the classification. Fortunately the classification algorithm proved useful for determined if a given team would win or lose with a high degree of certainty. 
If we had had more data, and more varied data from different seasons, the apriori algorithm might have been able to find a frequent pattern. Due to the high number of champions in the game (100+), a thousand matches were not enough for finding a frequent pattern using either apriori and GSP with a high support. 
Beside this, we can conclude that while we were not able to find useful frequent patterns, classification based on which objectives were achieved by a given team is enough for predicting with a precision of 90 %. 
We would have liked if we had had time to classify on the strengths of one team’s champions versus the other teams. We would also have liked to acquire more data for the frequent pattern mining.
