## Classification - ID3

With classification we wanted to classify on winning or losing the match. 
The classification algorithm was implemented with the expectation that the type of target class was a boolean. However, the properties of data tuple (team) could be of type boolean or enum. As long as only two teams participate in a match, and tie outcome is not possible, the boolean satisfies the needs. 
The attribute selection method is generalized to be able to handle the attributes that have a binary splitting criteria as well as multiple. More specifically, it dynamically switches between the calculation approaches of ID3 and C4.5 algorithms. 

The classification process consists of two steps: the first is data learning (or training phase), while the second is data classification (or testing phase). 

The result of training phase is a tree with multiple amount of paths that lead from the root to leaf nodes, having a unique set (vector) of attributes.
The grow of the path is terminated if it meets any of three different conditions:
·         All tuples are of the same class (if no pre-pruning applied);
·         A path has no more attributes to split on;
·         If a partition (successor branch) is empty.
In case the path has reached a terminal state, the node becomes a leaf with an appropriate label, otherwise the algorithm continues the path grow. Every non-leaf node represents an attribute and its value of a set of tuples. Such node properties are considered as the rule.

The second phase is designed to validate the model, which is derived by training the data. The result of this phase is a number of metrics that describe classifier evaluation performance. If that metrics satisfy the expectation, then the model rules are considered as accurate, and can be applied for predicting the class of the new data.

## Results:

The set of tuples consists of 2000 entries, each tuple is an object that represents a team with a number of attributes (fields). Three fourths of tuples are used for building the model, and the last quarter is used for model validation.

The generated tree of the model can be found in the appendix, in the file “Tree - isWinner.txt”.

Due to the number of winning and losing outcomes is even, it is not very helpful to use the baseline ZeroR method. The diagram 1 holds the data of the ZeroR baseline. That data is derived from classification part, and based on 500 data samples.


![Diagram 1. The ZeroR Confusion Matrix ](./img/ZeroR-Confusion-Matrix.png)

![Diagram 2. Classification Confusion Matrix \label{Classification Confusion Matrix}](./img/Classification-Confusion-Matrix.png)

We achieved an accuracy of 0.9, which means that our classification model is very good at predicting if a given team won or lost. It should be noted though that we have an error rate of 10 %, which is significant, but not high. For our purpose the error rate is acceptable. From diagram 2 we can see that the sensitivity and the specificity are very similar. It seems that we are not significantly better at predicting losses or wins. 
Our precision of 0.91 indicates that a high percentage of the teams labelled as wins actually are wins. We were able to achieve this high precision by only using 8 attributes. 

Based on our classification tree it would seem that if you play aggressively and try to take as many objectives as possible, you will most likely win. It seems that playing passively and letting the enemy take the objectives will result in a loss for your team.There are some outliers, which most likely is the reason that our tree only have an accuracy of 90 %. 
Also according to our tree, it would seem that most matches end up with one team taking the lead and eventually winning.
