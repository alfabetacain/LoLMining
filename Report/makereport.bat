pandoc -S --template=template.latex -o report.pdf ^
markdown/00_Frontpage_And_TOC.md ^
markdown/01_Introduction/01-Introduction.md ^
markdown/01_Introduction/02-TheData.md ^
markdown/01_Introduction/03-Algorithms.md ^
markdown/02_Results/00-Frequent-pattern-search-Apriori.md ^
markdown/02_Results/01-Frequent-pattern-Generalized-Sequential-Pattern-algorithm-(GSP).md ^
markdown/02_Results/02-Classification-ID3.md ^
markdown/02_Results/03-Conclusion.md ^
markdown/03_Appendix/01-Appendix.md