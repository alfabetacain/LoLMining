package main.championApriori;

/**
 * Based on lab session, but changed to accommodate strings instead of ints
 * @author christian
 *
 */
public class ItemSet {
	
    public final String[] set;

    public ItemSet( String[] set ) {
        this.set = set;
    }

    @Override
    /**
     * hashcode, must be the result of the sum of all strings in the set. It is assumed
     * that the set is sorted
     */
    public int hashCode() {
    	StringBuilder builder = new StringBuilder();
    	for (String s : set)
    		builder.append(s);
    	return builder.toString().hashCode();
    }

    
    @Override
    /**
     * Used to determine whether two ItemSet objects are equal
     */
    public boolean equals(Object o ) {
        if (!(o instanceof ItemSet)) {
            return false;
        }
        ItemSet other = (ItemSet) o;
        if (other.set.length != this.set.length) {
            return false;
        }
        for (int i = 0; i < set.length; i++) {
            if (!set[i].equals(other.set[i])) {
                return false;
            }
        }
        return true;
    }

    //nicer print method
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
		boolean isFirst = true;
        for (String i : set)
        {
			if (!isFirst) {
				sb.append(",");
			}
			else
				isFirst = false;
            sb.append(i);
        }
        sb.append("]");
        return sb.toString();
    }
}
