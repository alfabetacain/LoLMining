package main.championApriori;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Program structure based on lab sessions
 * @author christian
 *
 */
public class Apriori {
	
    public List<HashMap<ItemSet,Integer>> patternify(String[][] dataSet, int supportTreshold) {
    	return apriori(dataSet,supportTreshold);
    }

    public List<HashMap<ItemSet,Integer>> apriori( String[][] allSamples, int supportThreshold) {

        HashMap<ItemSet, Integer> frequentItemSets = generateFrequentItemSetsLevel1( allSamples, supportThreshold); // generate frequent item sets of size 1
        List<HashMap<ItemSet,Integer>> allFrequentSets = new ArrayList<>(); // a list containing the frequent item sets for each size
        allFrequentSets.add(frequentItemSets);
        //main loop, continue as long as frequent item sets can be generated
        while  (frequentItemSets.size() > 0) 
        {
            frequentItemSets = generateFrequentItemSets(supportThreshold, allSamples, frequentItemSets); //generate frequent item sets
            allFrequentSets.add(frequentItemSets); //add them to the complete list
        }
        return allFrequentSets;
    }

    /**
     * Method for generating frequent item sets
     * Generates item sets then prunes them to remove the infrequent ones
     * @param supportThreshold The minium support to be frequent item set
     * @param allSamples all samples
     * @param lowerLevelItemSets the item sets to generate the new item sets from
     * @return
     */
    private HashMap<ItemSet, Integer> generateFrequentItemSets(int supportThreshold, String[][] allSamples,
                    HashMap<ItemSet, Integer> lowerLevelItemSets) {

		HashMap<ItemSet,Integer> map = new HashMap<>();
		
    	//generate the item sets from the lower level item sets
		for(ItemSet set: lowerLevelItemSets.keySet())
		{
			for(ItemSet set2 : lowerLevelItemSets.keySet())
			{
				if( set.equals(set2)) continue; //no reason to join an item set with itself
				ItemSet newset = joinSets(set,set2); //join the sets to create a new item set, will return null if unable to join
				if (newset != null) {
					map.put(newset,0);
				}
			}
		}
		
		
		//prune infrequent item sets using the apriori property (if an item set has an infrequent subset, it cannot be frequent)
        Iterator<Entry<ItemSet, Integer>> iter = map.entrySet().iterator();
      	while(iter.hasNext())
      	{
      		Entry<ItemSet,Integer> entry = iter.next();
      		if ( shouldPrune(entry.getKey(), lowerLevelItemSets)) 
      			iter.remove();
      	}

      	//check if the new item sets are actually frequent
		iter = map.entrySet().iterator();
		while (iter.hasNext())
		{
			Entry<ItemSet,Integer> entry = iter.next();
			int support = countSupport(entry.getKey().set,allSamples);
			if (support > supportThreshold)
				map.put(entry.getKey(),support);
			else
				iter.remove();
		}
        return map; //returns the frequent ones
    }
	
    /**
     * Checks if a given item set has infrequent subsets by checking for subsets in the map
     * @param candidate the candidate to check for infrequent item sets
     * @param frequentItemSets a map of frequent item sets
     * @return true if candidate has infrequent subsets, false otherwise
     */
	private boolean shouldPrune(ItemSet candidate, Map<ItemSet,Integer> frequentItemSets)
	{
		int size = candidate.set.length-1;
  		String[] candidateSet = candidate.set;
  		String[] subset = new String[size];

  		//progressively removes one element from the set and checks if the resulting subset is frequent
  		for (int i = 0; i < candidateSet.length; i++) {
  			System.arraycopy(candidateSet, 0, subset, 0, i);
  			System.arraycopy(candidateSet, i+1, subset, i, candidateSet.length-i-1);
  			ItemSet newset = new ItemSet(subset); //make it an item set so we can search for it in the map
  			if (!frequentItemSets.containsKey(newset))
  				return true;
  		}
  		return false;
	}

	/**
	 * Joins two sets if they are of the same length and that the first length-1 elements are equal for both sets
	 * @param first the first set
	 * @param second the second set
	 * @return a new joined set of length+1, still ordered
	 */
    private ItemSet joinSets(ItemSet first, ItemSet second ) {
		if(first.set.length != second.set.length) return null; //cannot join two sets of differing lengths
		
		int length = first.set.length;
		
		for(int i = 0; i < first.set.length-1; i++)
			if(!first.set[i].equals(second.set[i])) return null; //checks the first length-1 elements, they must be equal
		
		String[] newset = new String[first.set.length+1];
		System.arraycopy(first.set, 0, newset, 0, first.set.length-1);
		
		//determine which of the end elements should be the last and the second last elements of the new joined set
		if( first.set[first.set.length-1].compareTo(second.set[second.set.length-1]) < 0) {
			newset[newset.length-2] = first.set[length-1];
			newset[newset.length-1] = second.set[length-1];
		}
		else
		{
			newset[newset.length-2] = second.set[length-1];
			newset[newset.length-1] = first.set[length-1];
		}
		return new ItemSet(newset);
    }

    /**
     * Generate frequent item sets of length 1
     * @param allSamples all samples
     * @param supportThreshold the threshold an item set must be above to be frequent
     * @return
     */
    private HashMap<ItemSet, Integer> generateFrequentItemSetsLevel1( String[][] allSamples, int supportThreshold) {
		HashMap<ItemSet,Integer> map = new HashMap<>();
		
		//generate length 1 sets
		for(int i = 0; i<allSamples.length; i++)
		{
			for(int k = 0; k<allSamples[i].length; k++)
			{
				ItemSet set = new ItemSet( new String[]{allSamples[i][k]});
				if (map.containsKey(set))
					map.put(set,map.get(set)+1);
				else
					map.put(set,1);
			}
		}
		//remove infrequent sets
		Iterator<Entry<ItemSet,Integer>> iterator = map.entrySet().iterator();
		while ( iterator.hasNext())
		{
			Entry<ItemSet,Integer> entry = iterator.next();
			if(entry.getValue() < supportThreshold) {
				iterator.remove();
			}
		}
        return map;
    }

    /**
     * count the support of the given item set
     * @param itemSet the item set to count for
     * @param transactions the complete data set to look in
     * @return the support count
     */
    private int countSupport( String[] itemSet, String[][] transactions ) {
        // Assumes that items in ItemSets and transactions are both unique
		int count = 0;
		for (int i = 0; i < transactions.length; i++)
		{
			if( contains(itemSet, transactions[i])) count++;
		}
        return count;
    }
	
    /**
     * checks if the containee is contained within the container
     * @param containee the subset
     * @param container the superset
     * @return true if containee is a subset of container
     */
	public static boolean contains(String[] containee, String[] container)
	{
		return Arrays.asList(container).containsAll(Arrays.asList(containee));
	}

}
