package main.championApriori;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import main.apriori.MatchWithChamps;
import main.loading.Loader;

public class ChampionApRunner {

	String[][] firstOrderOutput;
	String[][] secondOrderOutput;

	public ChampionApRunner() {

		Loader.Start();
		List<MatchWithChamps> matches = Loader.getMatches().stream()
				.map(match -> new MatchWithChamps(match))
				.collect(Collectors.toList());
		// [team Champ, Winner?]
		// composeFirstOrderStructure(matches);

		// [Cham teamOne, All Cham teanTwo, Winning Team]
		composeSecondOrderStructure(matches);
	}

	private void composeSecondOrderStructure(List<MatchWithChamps> matches) {
		List<String[]> data = new ArrayList<>();

		for (MatchWithChamps match : matches) {
			String[] temp = new String[11];
			for (int i = 0; i < 10; i++) {
				String name = i < 5 ? "TEAM ONE " : "TEAM TWO ";
				temp[i] = match.champMap.get(match.match.getParticipants()
						.get(i).getParticipantId())
						+ name;
			}
			temp[10] = match.match.getTeams().get(0).getWinner() ? "Team One Winner"
					: "Team Two Winner";
			Arrays.sort(temp);
			// printPrettyArray(temp);
			data.add(temp);

		}
		secondOrderOutput = data.toArray(new String[data.size()][]);

	}

	private void composeFirstOrderStructure(List<MatchWithChamps> matches) {
		List<String[]> data = new ArrayList<>();
		for (MatchWithChamps match : matches) {
			String[] teamOne = new String[6];
			String[] temp = new String[5];
			teamOne[5] = match.match.getTeams().get(0).getWinner() ? "WINNER"
					: "LOSER";
			for (int i = 0; i < 5; i++) {
				temp[i] = match.champMap.get(match.match.getParticipants()
						.get(i).getParticipantId());
			}
			Arrays.sort(temp);
			System.arraycopy(temp, 0, teamOne, 0, 5);
			data.add(teamOne);
			String[] teamTwo = new String[6];
			teamTwo[5] = match.match.getTeams().get(1).getWinner() ? "WINNER"
					: "LOSER";
			for (int i = 0; i < 5; i++) {
				temp[i] = match.champMap.get(match.match.getParticipants()
						.get(i + 5).getParticipantId());
			}
			Arrays.sort(temp);
			System.arraycopy(temp, 0, teamTwo, 0, 5);
			data.add(teamTwo);
			// printPrettyArray(teamOne);
			// printPrettyArray(teamTwo);
		}

		firstOrderOutput = data.toArray(new String[data.size()][]);
	}

	private void printPrettyArray(String[] input) {

		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (int i = 0; i < input.length; i++) {
			sb.append(input[i]);
			sb.append(" ");
		}
		sb.append(']');

		System.out.println(sb);

	}

	public String[][] getData() {
		return firstOrderOutput;
	}

	public String[][] getDataSecond() {

		return secondOrderOutput;
	}

	public static void main(String[] arg) {

		int supportThreshold = 1;
		double minimumConfidence = 0.9;
		ChampionApRunner collect = new ChampionApRunner();

		Apriori ap = new Apriori();

		List<HashMap<ItemSet, Integer>> complete = ap.apriori(
				collect.getDataSecond(), supportThreshold);
		// generating association rules
		for (int i = 1; i < complete.size(); i++) {
			for (Entry<ItemSet, Integer> superset : complete.get(i).entrySet()) {
				for (int j = i - 1; j >= 0; j--) {
					for (Entry<ItemSet, Integer> subset : complete.get(j)
							.entrySet()) {
						// if the subset is a proper subset generate association
						// rules
						if (!subset.getKey().equals(superset.getKey())
								&& Apriori.contains(subset.getKey().set,
										superset.getKey().set)) {

							// since the subset is a proper subset then the
							// support of the union must be the support of the
							// superset
							double confidence = ((double) superset.getValue())
									/ subset.getValue();
							if (confidence > minimumConfidence) {
								// pretty printing, remove the subset from the
								// superset and then print them
								List<String> reducedSuperSet = new ArrayList<String>(
										Arrays.asList(superset.getKey().set
												.clone()));
								reducedSuperSet.removeAll(Arrays.asList(subset
										.getKey().set));
								String[] array = (String[]) reducedSuperSet
										.toArray(new String[reducedSuperSet
												.size()]);
								Arrays.sort(array);
								ItemSet newSet = new ItemSet(array); // make it
																		// an
																		// itemset
																		// just
																		// to
																		// get
																		// the
																		// pretty
																		// tostring
																		// method
								System.out.println(subset.getKey() + "("
										+ subset.getValue() + ")" + " -> "
										+ newSet + "(" + superset.getValue()
										+ ")" + "=" + confidence);
							}
						}
					}
				}
			}
		}

	}
}
