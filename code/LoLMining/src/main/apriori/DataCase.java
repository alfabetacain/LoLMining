package main.apriori;

public class DataCase {
	public final EventAdapter hashedEvent;
	public final Sequence dataSequence;
	public final long timestamp;
	//private final String stringified;
	
	public DataCase(EventAdapter hashedEvent, Sequence dataSequence, long timestamp) {
		super();
		this.hashedEvent = hashedEvent;
		this.dataSequence = dataSequence;
		this.timestamp = timestamp;
	}
	
	@Override
	public int hashCode() {
		
		return toString().hashCode();
	}
	
	@Override
	public String toString() {

		StringBuilder b = new StringBuilder();
		b.append(hashedEvent == null ? "" : hashedEvent.toString());
		b.append(" from ");
		b.append(dataSequence.hashCode());
		b.append(" at time ");
		b.append(timestamp);
		return b.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof DataCase) {
			DataCase other = (DataCase) o;
			return hashCode() == other.hashCode();
		}
		return false;
	}
}
