package main.apriori;

public class FoundFrame {
	public final FrameAdapter frame;
	public final long startTime;
	public final long endTime;
	
	public FoundFrame(FrameAdapter frame, long startTime, long endTime) {
		super();
		this.frame = frame;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	@Override
	public String toString() {
		return frame.toString() + " -> " + startTime + " - " + endTime;
	}
}
