package main.apriori;

public final class Stats {
	private Stats() {}
	
	public static final int WINDOW_SIZE = 1;
	public static final int MAX_GAP = 100000;
	public static final int MINIMUM_SUPPORT = 500;
	public static final double CONFIDENCE = 0.75;
}
