package main.apriori;

import java.util.HashMap;
import java.util.Map;

import main.loading.Loader;

import com.robrua.orianna.type.core.staticdata.Champion;
import com.robrua.orianna.type.dto.match.MatchDetail;
import com.robrua.orianna.type.dto.match.Participant;

public class MatchWithChamps {
	public final MatchDetail match;
	public final Map<Integer, String> champMap;
	
	public MatchWithChamps(MatchDetail match) {
		super();
		this.match = match;
		this.champMap = new HashMap<>();
		for (Participant participant : match.getParticipants()) {
			Champion champ = Loader.getChampion(participant.getChampionId());
			champMap.put(participant.getParticipantId(), champ.getName());
		}
	}
	
	
}
