package main.apriori;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import com.robrua.orianna.type.dto.match.MatchDetail;

public class Aprior {
	
	private static final ExecutorService threadPool = Executors.newFixedThreadPool(1);
	
	 public NewNode patternify(List<MatchDetail> dataset) {
		 	System.out.println(Runtime.getRuntime().maxMemory());
	    	return apriori(dataset.stream().map(match -> new Sequence(match)).collect(Collectors.toList()));
	    }

	    public NewNode apriori( List<Sequence> allSamples) {

	    	NewNode tree = generateFrequentItemSetsLevel1And2(allSamples);
	    	Collection<Sequence> old = tree.getSupportedOnLevel(2);
	    	int iteration = 3;
	    	tree.printSize();
	    	while (old.size() > 0) {
	    		System.out.println("Generating lvl " + iteration);
	    		System.out.println("no = " + old.size());
	    		generateFrequentItemSets(tree,old, iteration);
	    		old = tree.getSupportedOnLevel(iteration++);
	    		tree.printSize();
	    		System.out.println("Starting new run...");
	    	}
	    	System.out.println("Returning...");
	    	for (int i = 2; i < iteration; i++) {
	    		for (Sequence big : tree.getSupportedOnLevel(i)) {
	    			for (Sequence small : generateSubSequences(big)) {
	    				double bigSupport = tree.getSupport(big);
	    				double smallSupport = tree.getSupport(small);
	    				double confidence = bigSupport / smallSupport;
	    				if (confidence > Stats.CONFIDENCE) {
	    					System.out.println(small + " -> " + big + " (" + confidence + ")");
	    				}
	    			}
	    		}
	    	}
	    	threadPool.shutdownNow();
	    	return tree;
	    }

	    /**
	     * Method for generating frequent item sets
	     * Generates item sets then prunes them to remove the infrequent ones
	     * @param supportThreshold The minium support to be frequent item set
	     * @param allSamples all samples
	     * @param lowerLevelItemSets the item sets to generate the new item sets from
	     * @return
	     * @throws ExecutionException 
	     */
	    private void generateFrequentItemSets(NewNode tree, Collection<Sequence> lower, int updateLevel) {
			
			Set<Sequence> candidates = new HashSet<>();
	    	//generate the item sets from the lower level item sets
			System.out.println("joining " + lower.size());
//			List<Callable<Sequence>> futures = new ArrayList<>();
			for(Sequence set: lower)
			{
				for(Sequence set2 : lower)
				{
					if( set.equals(set2)) continue; //no reason to join an item set with itself
					Sequence newSeq = joinSets(set,set2);
					if (newSeq != null && !shouldPrune(newSeq, tree))
						candidates.add(newSeq);
//					futures.add(new Callable<Sequence>() {
//						public Sequence call() {
//							Sequence newSeq = joinSets(set,set2);
//							if (newSeq != null && !shouldPrune(newSeq, tree)) {
//								return newSeq;
//							}
//							return null;
//						}
//					});
					//Sequence newset = joinSets(set,set2); //join the sets to create a new item set, will return null if unable to join
//					if (newset != null) {
//						candidates.add(newset);
//					}
				}
			}
//			Set<Sequence> sequences = null;
//			try {
//				System.out.println("Executing joins...");
//				List<Future<Sequence>> fs = threadPool.invokeAll(futures);
//				futures = null;
//				System.out.println("Done executing, now filtering...");
//				sequences = fs.parallelStream().map(future -> {
//					try {
//						Sequence seq = future.get();
//						if (seq == null) return null;
//						//System.out.println(seq + " ---> " + seq.hashCode());
//						return seq;
//					} catch (InterruptedException | ExecutionException e) {
//						System.out.println("Exception occurred");
//						return null;
//					}
//				}).filter(seq -> seq != null).collect(Collectors.toSet());
//			}
//			catch (InterruptedException e) {
//				throw new RuntimeException("Should not fail");
//			}
////			candidates = sequences;
//			sequences = null;
			
//			System.out.println("pruning " + candidates.size());
//			candidates = candidates.parallelStream().filter(seq -> !shouldPrune(seq,tree)).collect(Collectors.toSet());
//			Iterator<Sequence> iterator = candidates.iterator();
//	      	while(iterator.hasNext())
//	      	{
//	      		Sequence next = iterator.next();
//	      		if ( shouldPrune(next, tree)) 
//	      			iterator.remove();
//	      	}
			System.out.println("Adding " + candidates.size() + " to tree...");
			for (Sequence candidate : candidates)
				tree.addCandidate(candidate);
			candidates = null;
			System.out.println("Updating tree...");
			tree.update(updateLevel);
	      	//tree.addCandidates(candidates);
	      	System.out.println("Done adding");
	    }
		
	    /**
	     * Checks if a given item set has infrequent subsets by checking for subsets in the map
	     * @param candidate the candidate to check for infrequent item sets
	     * @param frequentItemSets a map of frequent item sets
	     * @return true if candidate has infrequent subsets, false otherwise
	     */
		private boolean shouldPrune(Sequence candidate, NewNode tree)
		{
//			System.out.println("Testing: " + candidate);
			List<Sequence> subSequences = generateSubSequences(candidate);
//			System.out.println("Mother: " + candidate);
//			for (Sequence subSequence : subSequences) {
////				System.out.println("Subsequence: " + subSequence);
//				int support = tree.getSupport(subSequence);
////				System.out.println("Support: " + support);
//				if (support < Stats.MINIMUM_SUPPORT) {
//					//System.out.println(subSequence);
//					return true;
//				}
//			}
//			return false;
			return subSequences.parallelStream().anyMatch(seq -> tree.getSupport(seq) < Stats.MINIMUM_SUPPORT);
		}
		
		private List<Sequence> generateSubSequences(Sequence candidate) {
			List<Sequence> sequences = new ArrayList<>();
			sequences.addAll(generateSubsequenceByRemovingFromSetIndex(candidate, 0));
			sequences.addAll(generateSubsequenceByRemovingFromSetIndex(candidate, candidate.getSequence().size()-1));
			for (int i = 1; i < candidate.getSequence().size()-1; i++) {
				if (candidate.get(i).getEvents().size() > 2)
					sequences.addAll(generateSubsequenceByRemovingFromSetIndex(candidate, i));
			}
			return sequences;
		}
		
		private List<Sequence> generateSubsequenceByRemovingFromSetIndex(Sequence candidate, int index) {

			List<Sequence> sequences = new ArrayList<>();
			for (EventAdapter event : candidate.get(index).getEvents()) {
				List<EventAdapter> newSet = candidate.get(index).getEvents().stream().filter(item -> !item.equals(event)).collect(Collectors.toList());
				FrameAdapter newAdapter = new FrameAdapter(candidate.get(index).getInner(), newSet);
				ArrayList<FrameAdapter> newSeq = new ArrayList<>();
				newSeq.addAll(candidate.getSequence().subList(0, index));
				if (newAdapter.size() != 0)
					newSeq.add(newAdapter);
				newSeq.addAll(candidate.getSequence().subList(index+1, candidate.getSequence().size()));
				Sequence newFrameSeq = new Sequence(newSeq);
				sequences.add(newFrameSeq);
			}
			return sequences;
		}

		/**
		 * Joins two sets if they are of the same length and that the first length-1 elements are equal for both sets
		 * @param first the first set
		 * @param second the second set
		 * @return a new joined set of length+1, still ordered
		 */
	    private Sequence joinSets(Sequence first, Sequence second ) {
			//if(first.getSequence().size() != second.getSequence().size()) return null; //cannot join two sets of differing lengths
			
//			System.out.println("Mother first: " + first);
//			System.out.println("Mother second: " + second);
			Sequence shorterFirst = getWithoutItem(true, first);
			Sequence shorterSecond = getWithoutItem(false, second);
			//System.out.println("Length of first: " + shorterFirst.size());
			//System.out.println("Length of second: " + shorterSecond.size());
//			System.out.println("Comparing------------------------------------------------------");
//			System.out.println(shorterFirst);
//			System.out.println(shorterSecond);
//			System.out.println("-------------------------------------------------------");
			if (!shorterFirst.equals(shorterSecond)) return null;
			//if gone this far, they share a subsequence
			int firstSize = first.getSequence().size();
			int secondSize = second.getSequence().size();
			
			List<FrameAdapter> newseq = new ArrayList<>();
			newseq.addAll(first.getSequence().subList(0, firstSize-1));
			if (second.getLastFrame().getEvents().size() == 1) {
				//append as new frame
				newseq.add(first.getSequence().get(firstSize-1));
				newseq.add(second.getSequence().get(secondSize-1));
			}
			else {
				//append to last frame's set
				List<EventAdapter> newEvents = new ArrayList<>();
				newEvents.addAll(first.getLastFrame().getEvents().subList(0, first.getLastFrame().getEvents().size()-1));
				newEvents.add(second.getLastFrame().getEvents().get(second.getLastFrame().getEvents().size()-1));
				FrameAdapter adapter = new FrameAdapter(null, newEvents);
				newseq.add(adapter);
			}
			Sequence newSequence = new Sequence(newseq);
//			System.out.println("New sequence: " + newSequence);
			//System.out.println("Length of generated sequence: " + newSequence.size());
//			if (newSequence.size() != first.size() + 1)
//				System.out.println("Newly generated sequence is too long");
			//if (newSequence.size() != first.size() + 1) throw new RuntimeException("Newly generated sequence is too long");
			return newSequence;
	    }

	    /**
	     * Generate frequent item sets of length 1
	     * @param allSamples all samples
	     * @param supportThreshold the threshold an item set must be above to be frequent
	     * @return
	     */
	    private NewNode generateFrequentItemSetsLevel1And2( List<Sequence> allSamples) {
	    	System.out.println("Generating lvl 1");
			NewNode tree = new NewNode(1);
			//tree.addAnswers(allSamples);
			
			//generate the first single element sets
			Set<EventAdapter> events = new HashSet<>();
			for (Sequence seq : allSamples) {
				for (FrameAdapter frame : seq.getSequence()) {
					events.addAll(frame.getEvents());
				}
			}
			List<Sequence> singleSeqs = events.stream().map(event -> new Sequence(new FrameAdapter(null, event))).collect(Collectors.toList());
//			for (Sequence seq : singleSeqs)
//				System.out.println(seq);
			for (Sequence single : singleSeqs)
				tree.addCandidate(single);
			for (Sequence data : allSamples)
				tree.addDataSequence(data);
			tree.update(1);
			//tree.addCandidates(singleSeqs);
			System.out.println("Number of candidates of size 1: " + singleSeqs.size());
			System.out.println("Generating lvl 2");
			Collection<Sequence> length2Seqs = new ArrayList<>();
			Collection<Sequence> frequentOnes = tree.getSupportedOnLevel(1);
			System.out.println("Valid 1's for generating of two's = " + frequentOnes.size());
			for (Sequence seq1 : frequentOnes) {
				for (Sequence seq2 : frequentOnes) {
					if (!seq1.get(0).getEvents().get(0).equals(seq2.get(0).getEvents().get(0))) {
						List<EventAdapter> temp = new ArrayList<>();
						temp.add(seq1.get(0).getEvents().get(0));
						temp.add(seq2.get(0).getEvents().get(0));
						Collections.sort(temp);
						length2Seqs.add(new Sequence(new FrameAdapter(null, temp)));
					}
					FrameAdapter frame1 = new FrameAdapter(null, seq1.get(0).getEvents());
					FrameAdapter frame2 = new FrameAdapter(null, seq2.get(0).getEvents());
					ArrayList<FrameAdapter> frames = new ArrayList<>();
					frames.add(frame1);
					frames.add(frame2);
					length2Seqs.add(new Sequence(frames));
				}
			}
			System.out.println("Twos generated, adding to tree " + length2Seqs.size() + "...");
			
			for (Sequence cand : length2Seqs)
				tree.addCandidate(cand);
			System.out.println("Updating tree...");
			tree.update(2);
			//tree.addCandidates(length2Seqs);
			
			return tree;
	    }
		
		private Sequence getWithoutItem(boolean removeFirst, Sequence sequence) {
			if (removeFirst) {
				FrameAdapter first = sequence.get(0);
				List<EventAdapter> newList = first.getEvents().subList(1, first.getEvents().size());
				FrameAdapter newFrame = new FrameAdapter(null, newList);
				List<FrameAdapter> newFrames = new ArrayList<>();
				if (newFrame.getEvents().size() > 0)
					newFrames.add(newFrame);
				newFrames.addAll(sequence.getSequence().subList(1, sequence.getSequence().size()));
				return new Sequence(newFrames);
			}
			else {
				if (sequence.getLastFrame().size() == 1)
					return new Sequence(sequence.getSequence().subList(0, sequence.getSequence().size()-1));
				else {
					FrameAdapter last = sequence.getLastFrame();
					List<EventAdapter> newList = last.getEvents().subList(0, last.getEvents().size()-1);
					FrameAdapter newFrame = new FrameAdapter(null, newList);
					ArrayList<FrameAdapter> newFrames = new ArrayList<>();
					newFrames.addAll(sequence.getSequence().subList(0, sequence.getSequence().size()-1));
					newFrames.add(newFrame);
					return new Sequence(newFrames);
				}
			}
//			int frameIndex = removeFirst ? 0 : sequence.getSequence().size()-1;
//			List<FrameAdapter> frames = sequence.getSequence();
//			List<EventAdapter> events = frames.get(frameIndex).getEvents();
//			int startItemIndex = removeFirst ? 1 : 0;
//			int endItemIndex = removeFirst ? events.size()-1 : events.size()-2;
//			int startFrameIndex = removeFirst ? 1 : 0;
//			int endFrameIndex = removeFirst ? sequence.size()-1 : sequence.size()-2;
//			
//			ArrayList<FrameAdapter> newFirstFrames = new ArrayList<>();
//			if (events.size() != 1) {
//				ArrayList<EventAdapter> newEvents = new ArrayList<>();
//				newEvents.addAll(events.subList(startItemIndex, endItemIndex));
//				newFirstFrames.add(new FrameAdapter(frames.get(frameIndex).getInner(),newEvents));
//			}
//			newFirstFrames.addAll(frames.subList(startFrameIndex, endFrameIndex));
//			return new Sequence(newFirstFrames);
		}

	}
