package main.apriori;

import com.robrua.orianna.type.dto.match.Event;
import com.robrua.orianna.type.dto.match.MatchDetail;

public class EventAdapter implements Comparable<EventAdapter> {
	private final Event inner;
	private final EventTypeAdapter type;
	private final int hash;
	private static final String TEAM_ONE = "TEAM ONE";
	private static final String TEAM_TWO = "TEAM TWO";
	private String s;

	public EventAdapter(Event inner, MatchWithChamps match) {
		super();
		this.inner = inner;
		type = EventTypeAdapter.valueOf(inner.getEventType().toString());
		hash = setup(match);
	}
	
	public EventAdapter(MatchDetail match) {
		type = EventTypeAdapter.GAME_ENDED;
		if (match.getTeams().get(0).getWinner())
		{
			s = type.toString() + TEAM_ONE;
			hash = s.hashCode();
		}
		else
		{
			s = type.toString() + TEAM_TWO;
			hash = s.hashCode();
		}
		inner = null;
	}
	
	private int setup(MatchWithChamps match) {
		StringBuilder b = new StringBuilder();
		b.append(type);
		switch (type) {
		case ASCENDED_EVENT:
			b.append(inner.getKillerId() < 6 ? TEAM_ONE : TEAM_TWO);
			b.append(inner.getAscendedType());
			b.append(inner.getAssistingParticipantIds() == null ? 1 : inner.getAssistingParticipantIds().size() + 1);
			break;
		case BUILDING_KILL:
			b.append(inner.getKillerId() < 6 ? TEAM_ONE : TEAM_TWO);
			b.append(inner.getBuildingType());
			b.append(inner.getTowerType());
			break;
		case CHAMPION_KILL:
			b.append(inner.getKillerId() < 6 ? TEAM_ONE : TEAM_TWO);
			b.append(inner.getAssistingParticipantIds() == null ? 1 : inner.getAssistingParticipantIds().size() + 1);
			break;
		case ELITE_MONSTER_KILL:
			b.append(inner.getKillerId() < 6 ? TEAM_ONE : TEAM_TWO);
			b.append(inner.getMonsterType());
			break;
		case WARD_KILL: 
			b.append(inner.getKillerId() < 6 ? TEAM_ONE : TEAM_TWO);
			b.append(inner.getWardType());
			break;
		case WARD_PLACED:
			b.append(inner.getCreatorId() < 6 ? TEAM_ONE : TEAM_TWO);
			b.append(inner.getWardType());
			break;
		case GAME_ENDED:
			b.append(match.match.getTeams().get(0).getWinner() ? TEAM_ONE : TEAM_TWO);
			break;
		case ITEM_PURCHASED:
			b.append(inner.getParticipantId() < 6 ? TEAM_ONE : TEAM_TWO);
			b.append(inner.getItemId());
		default:
			break;
		}
		s = b.toString();
		//System.out.println("Event: " + s);
		return s.hashCode();
	}

	public EventTypeAdapter getType() {
		return type;
	}

	public Event getInner() {
		return inner;
	}
	
	public boolean isSuperSet(EventAdapter other) {
		return false;
	}

	@Override
	public int compareTo(EventAdapter o) {
		int typeCompare = type.toString().compareTo(o.getType().toString());
		if (typeCompare != 0) return typeCompare;
		
		//TODO Compare with inner information to make sure a total order exists
		return 0;
	}
	
	@Override
	public int hashCode() {
		return hash;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof EventAdapter) {
			EventAdapter other = (EventAdapter) o;
			return hashCode() == other.hashCode();
		}
		return false;
	}
	
	@Override
	public String toString() {
		return s;
	}
}
