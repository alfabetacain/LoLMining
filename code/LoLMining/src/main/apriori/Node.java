package main.apriori;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.OptionalLong;
import java.util.Set;
import java.util.stream.Collectors;

public class Node {
	private final HashMap<Sequence, Node> map;
	public final List<Wrapper> answers;
	private final int depth;
	
	public Node(int depth) {
		this.depth = depth;
		map = new HashMap<>();
		answers = new ArrayList<>();
	}
	
	public void addAnswers(List<Sequence> dataSequences) {
		answers.addAll(dataSequences.stream().map(seq -> new Wrapper(null,seq)).collect(Collectors.toList()));
	}
	
	public void addAnswers(Collection<Wrapper> answers) {
		answers.addAll(answers);
	}
	
	public synchronized void addAnswer(Wrapper answer) {
		answers.add(answer);
	}
	
	public void addCandidates(Collection<Sequence> candidates) {
		for (Sequence candidate : candidates) {
			addCandidate(candidate);
		}
	}
	
	public int getSupport(Sequence seq) {
		Sequence subSequence = seq.getSubSequence(depth);
		if (seq.size() == depth-1) {
			if (map.containsKey(subSequence))
				return map.get(subSequence).answers.size();
			else return 0;
		}
		else if (!map.containsKey(subSequence)) {
			OptionalLong opt = map.values().stream().mapToLong(node -> node.getSupport(seq)).max();
			return opt.isPresent() ? (int) opt.getAsLong() : 0;
		}
		else
			return map.get(subSequence).getSupport(seq);
	}
	
	public void addCandidate(Sequence candidate) {
		Sequence subSequence = candidate.getSubSequence(depth+1);
		if (!map.containsKey(subSequence)) {
			//System.out.println("Adding sequence: " + subSequence);
			Node newNode = new Node(depth+1);
			answers.parallelStream().map(ans -> isSubSequenceOf(candidate, ans)).filter(item -> item != null).forEach(wrap -> newNode.addAnswer(wrap));
			if (newNode.answers.size() >= Stats.MINIMUM_SUPPORT)
				map.put(subSequence, newNode);
		}
		else if (candidate.size() > depth+1){
			map.get(subSequence).addCandidate(candidate);
		}
	}
	
	public Node get(EventAdapter e) {
		return map.get(e);
	}
	
	public Collection<Sequence> getSupportedOnLevel(int level) {
		if (depth+1 == level) 
			return map.keySet().stream().filter(key -> map.get(key).answers.size() >= Stats.MINIMUM_SUPPORT).collect(Collectors.toList());
		else {
			ArrayList<Sequence> someList = new ArrayList<>();
			map.values().stream().map(node -> node.getSupportedOnLevel(level)).forEach(someList::addAll);
			return someList;
		}
	}
	
	public Wrapper isSubSequenceOf(Sequence candidate, Wrapper data) {
		if (candidate.getLastFrame().size() == 1) {
			//need to find new frame
			for (FrameAdapter frame : data.answer.getSequence()) {
				if (data.previous != null && frame.getTimestamp() <= data.previous.getTimestamp()) 
					continue;
				for (EventAdapter e : frame.getEvents()) {
					//System.out.println("Comparing: " + candidate.getLastFrame().getEvents().get(0) + " === " + e);
					if (e.equals(candidate.getLastFrame().getEvents().get(0))) {
						//System.out.println("Equal: " + candidate.getLastFrame().getEvents().get(0) + " === " + e);
						return new Wrapper(frame, data.answer);
					}
				}
//				if (frame.getEvents().stream().anyMatch(e -> e.equals(candidate.getLastFrame().getEvents().get(0)))) {
//					return new Wrapper(frame, data.answer);
//				}
			}
			return null;
		}
		else {
			EventAdapter lastItem = candidate.getLastFrame().getEvents().get(candidate.getLastFrame().getEvents().size()-1);
			for (FrameAdapter frame: data.answer.getSequence()) {
				if (data.previous != null && frame.getTimestamp() < data.previous.getTimestamp()) 
					continue;
				if (frame.getEvents().stream().anyMatch(e -> e.equals(lastItem)))
					return new Wrapper(frame, data.answer);
			}
			return null;
		}
	}
}
