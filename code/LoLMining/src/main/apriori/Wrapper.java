package main.apriori;

public class Wrapper {
	public final FrameAdapter previous;
	public final Sequence answer;
	
	public Wrapper(FrameAdapter previous, Sequence answer) {
		super();
		this.previous = previous;
		this.answer = answer;
	}
	
	
}
