package main.apriori;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.robrua.orianna.type.dto.match.Frame;

public class FrameAdapter implements Comparable<FrameAdapter> {
	private final Frame inner;
	private final List<EventAdapter> events;
	private long timestamp;
	private int hashCode;

	public FrameAdapter(Frame inner, MatchWithChamps match) {
		super();
		this.inner = inner;
		if (inner == null) System.out.println("Frame is null");
		Set<EventAdapter> set = inner.getEvents().stream().filter(e -> !e.getEventType().equals("CAPTURE_POINT") && !e.getEventType().equals("SKILL_LEVEL_UP") && !e.getEventType().equals("ITEM_DESTROYED") && !e.getEventType().equals("ITEM_SOLD") && !e.getEventType().equals("ITEM_UNDO")).map(event -> new EventAdapter(event, match)).collect(Collectors.toSet());
		events = new ArrayList<>(set);
		hashCode = calculateHashCode();
		//&& !e.getEventType().equals("ITEM_PURCHASED") 
	}
	
	public FrameAdapter(Frame frame, List<EventAdapter> events) {
		inner = frame;
		this.events = events;
		hashCode = calculateHashCode();
	}
	
	public FrameAdapter(long timestamp, EventAdapter e) {
		events = new ArrayList<>();
		events.add(e);
		this.timestamp = timestamp;
		inner = null;
		hashCode = calculateHashCode();
	}
	
	public long getTimestamp() {
		if (inner == null) 
			return timestamp;
		return inner.getTimestamp();
	}
	
	public void addEventAdapter(EventAdapter e) {
		events.add(e);
		hashCode = calculateHashCode();
	}
	
	private int calculateHashCode() {
		StringBuilder b = new StringBuilder();
		for (EventAdapter e : events)
			b.append(""+e.hashCode());
		return b.toString().hashCode();
	}
	
	public FrameAdapter(Frame frame, EventAdapter event) {
		inner = frame;
		events = new ArrayList<>();
		events.add(event);
		hashCode = calculateHashCode();
	}

	public Frame getInner() {
		return inner;
	}
	
	public List<EventAdapter> getEvents() {
		return events;
	}
	
	public int size() {
		return events.size();
	}

	public boolean isSubsetOf(FrameAdapter other) {
		return events.stream().allMatch(event -> other.getEvents().contains(events));
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public int compareTo(FrameAdapter o) {
		if (inner.getTimestamp() < o.getInner().getTimestamp())
			return -1;
		else if (inner.getTimestamp() > o.getInner().getTimestamp())
			return 1;
		else return 0;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("Frame: [");
		for (EventAdapter e : events) 
			b.append(e + " : ");
		b.append(" ]");
		return b.toString();
	}
}
