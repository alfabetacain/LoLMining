package main.apriori;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import com.robrua.orianna.type.dto.match.MatchDetail;

public class Sequence {
	private final List<FrameAdapter> sequence;
	private boolean hashHasBeenCalculated = false;
	private int hashCode = 0;
	private Map<EventAdapter,List<Long>> itemMap = null;
	private final int size;

	public Sequence(MatchDetail match) {
		MatchWithChamps matchDetails = new MatchWithChamps(match);
		List<FrameAdapter> tempFrames = match.getTimeline().getFrames().stream().filter(f -> f.getEvents() != null && f.getEvents().size() != 0).map(frame -> new FrameAdapter(frame,matchDetails)).sorted().collect(Collectors.toList());
		tempFrames = tempFrames.stream().filter(f -> f.getEvents().size() > 0).collect(Collectors.toList());
		//System.out.println("Largest frame: " + tempFrames.stream().mapToInt(f -> f.getEvents().size()).max().getAsInt());
		if (match.getMatchDuration() == tempFrames.get(tempFrames.size()-1).getInner().getTimestamp()) {
			tempFrames.get(tempFrames.size()-1).addEventAdapter(new EventAdapter(match));
		}
		else
			tempFrames.add(new FrameAdapter(match.getMatchDuration(),new EventAdapter(match)));
		this.sequence = tempFrames;
		size = calculateSize();//sequence.stream().map(frame -> "" + frame.hashCode()).reduce("",String::concat).hashCode();//match.getTimeline().getFrames().stream().filter(f -> f.getEvents() != null && f.getEvents().size() != 0).map(frame -> new FrameAdapter(frame,matchDetails)).sorted().collect(Collectors.toList());
	}
	
	public Sequence(FrameAdapter frame) {
		sequence = new ArrayList<>();
		sequence.add(frame);
		size = calculateSize();//sequence.stream().map(f -> "" + f.hashCode()).reduce("",String::concat).hashCode();//match.getTimeline().getFrames().stream().filter(f -> f.getEvents() != null && f.getEvents().size() != 0).map(frame -> new FrameAdapter(frame,matchDetails)).sorted().collect(Collectors.toList());
	}
	
	public Sequence(List<FrameAdapter> sequence) {
		this.sequence = sequence;//sequence.stream().map(f -> "" + f.hashCode()).reduce("",String::concat).hashCode();
		size = calculateSize();
	}
	
	private int calculateHash() {
		StringBuilder b = new StringBuilder();
		for (FrameAdapter frame : sequence) 
			b.append(frame.hashCode());
		return b.toString().hashCode();
	}

	public List<FrameAdapter> getSequence() {
		return sequence;
	}
	
	public FrameAdapter get(int index) {
		return sequence.get(index);
	}
	
	private int calculateSize() {
		return sequence.stream().mapToInt(frame -> frame.size()).sum();
	}
	
	public int size() {
		return size;
	}
	
	@Override
	public int hashCode() {
		if (!hashHasBeenCalculated) {
			hashCode = calculateHash();
			hashHasBeenCalculated = true;
		}
		return hashCode;//sequence.stream().map(frame -> "" + frame.hashCode()).reduce("",String::concat).hashCode();
	}
	
	public FrameAdapter getLastFrame() {
		return sequence.get(sequence.size()-1);
	}
	
	public Sequence getSubSequence(int max) {
		int size = size();
		if (size == max) return this;
		int taken = 0;
		int current = 0;
		ArrayList<FrameAdapter> frames = new ArrayList<>();
		while (taken < max) {
			if (sequence.get(current).size() + taken > max) {
				List<EventAdapter> events = new ArrayList<>();
				for (int i = 0; i < max-taken; i++) 
					events.add(sequence.get(current).getEvents().get(i));
				frames.add(new FrameAdapter(null, events));
				taken += events.size();
			}
			else {
				frames.add(sequence.get(current));
				taken += sequence.get(current).size();
			}
			current++;
		}
		return new Sequence(frames);
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("Sequence----------------\n");
		for (FrameAdapter frame : sequence)
		{
			b.append("          ");
			b.append(frame);
		}
		return b.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Sequence)
			return hashCode() == ((Sequence) o).hashCode();
		return false;
	}
	
	public EventAdapter getPthItem(int p) {
		int soFar = 0;
		for (int i = 0; i < sequence.size(); i++) {
			FrameAdapter frame = sequence.get(i);
			if (p < soFar + frame.size()) {
				//must be in this frame
				return frame.getEvents().get(p-soFar);
			}
			else
				soFar += frame.size();
		}
		return null;
	}
	
	public void setupItemMap() {
		itemMap = new HashMap<>();
		for (FrameAdapter frame : sequence) {
			for (EventAdapter event : frame.getEvents()) {
				if (itemMap.containsKey(event)) {
					itemMap.get(event).add(frame.getTimestamp());
				}
				else {
					List<Long> times = new ArrayList<>();
					times.add(frame.getTimestamp());
					itemMap.put(event, times);
				}
			}
		}
	}
	
	public FoundFrame findSingleElement(FrameAdapter frame, long fromTime) {
		if (itemMap == null) setupItemMap();
		Map<EventAdapter, Long> found = new HashMap<>();
		long startTime = fromTime;
		while (true) {
			found.clear();
			for (Entry<EventAdapter,List<Long>> entry : itemMap.entrySet()) {
				if (entry.getKey().equals(frame.getEvents().get(0))) {
					if (frame.getEvents().contains(entry.getKey())) {
						for (long time : entry.getValue()) {
							if (time >= startTime) {
								found.put(entry.getKey(), time);
								break;
							}
						}
					}
				}
			}
			if (found.size() != frame.getEvents().size())
				return null;
			long highest = found.values().stream().mapToLong(t -> t).max().getAsLong();
			long lowest = found.values().stream().mapToLong(t -> t).min().getAsLong();
			if (highest - lowest > Stats.WINDOW_SIZE) {
				startTime = highest - Stats.WINDOW_SIZE;
			}
			else {
				return new FoundFrame(frame,lowest,highest);
			}
		}
	}
	
	public boolean supports(Sequence candidate) {
//		System.out.println("-------------------------------------------------");
//		System.out.println("Candidate size: " + candidate.size);
		List<FoundFrame> foundSoFar = new ArrayList<>();
		Map<FoundFrame,Integer> foundIndex = new HashMap<>();
		long time = 0;
		for (int i = 0; foundSoFar.size() < candidate.size() && i < candidate.size(); i++) {
			FrameAdapter frame = candidate.get(i);
//			System.out.println("Searching for frame " + i + " : " + frame);
			FoundFrame found = findSingleElement(frame, time);
//			System.out.println("Found: " + found);
			if (found == null)
				return false;
			else if (foundSoFar.size() > 0 && Math.abs(foundSoFar.get(foundSoFar.size()-1).endTime - found.startTime) > Stats.MAX_GAP) { //max gap not satisfied
				//backwards
//				System.out.println("Max gap not satisfied");
				boolean backwards = true;
				long fromTime = found.endTime - Stats.MAX_GAP+1;
				while (backwards) {
					if (foundSoFar.size() == 0)
						return false;
					FoundFrame pulledUp = foundSoFar.get(foundSoFar.size()-1);
//					System.out.println("Pulled up: " + pulledUp);
					foundSoFar.remove(foundSoFar.size()-1);
					FoundFrame newlyFound = findSingleElement(pulledUp.frame, fromTime);
//					System.out.println("Newly found: " + newlyFound);
					if (newlyFound == null)
						return false;
					if (foundSoFar.size() == 0 || Math.abs(foundSoFar.get(foundSoFar.size()-1).endTime - newlyFound.startTime) <= Stats.MAX_GAP) { //can check max gap constraint
						//success!
//						System.out.println("NewlyFound satisfy max gap!");
						foundSoFar.add(newlyFound);
						int pulledUpIndex = foundIndex.remove(pulledUp);
						foundIndex.put(newlyFound, pulledUpIndex);
						i = pulledUpIndex;
						backwards = false;
						time = newlyFound.endTime + Stats.WINDOW_SIZE;
					}
					else { //max gap not satisfied
//						System.out.println("NewlyFound does not satisfy max gap, pulling the next one up...");
						foundIndex.remove(pulledUp);
						fromTime = newlyFound.endTime - Stats.MAX_GAP;
					}
				}
			}
			else {
//				System.out.println("Adding to foundSoFar...");
				//is the first element found or max gap is satisfied
				time = found.endTime + Stats.WINDOW_SIZE;
				foundSoFar.add(found);
				foundIndex.put(found, i);
			}
		}
		return true;
	}
}
