package main.apriori;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class NewNode {
	private final int depth;
	private HashMap<EventAdapter, NewNode> map = new HashMap<>();
	private Map<Sequence,Set<Sequence>> candidates = new HashMap<>();
	private Set<DataCase> dataSequences = new HashSet<>();
	
	public NewNode(int depth) {
		this.depth = depth;
	}
	
	public void addCandidate(Sequence candidate) {
		if (candidate.size() + 1 == depth) {
			if (!candidates.containsKey(candidate))
				candidates.put(candidate, new HashSet<>());
		}
		else if (candidate.size() + 1 > depth) { //send along if larger than the depth of this node
			EventAdapter pthItem = candidate.getPthItem(depth-1);
			if (!map.containsKey(pthItem)) {
				NewNode n = new NewNode(depth+1);
				n.addCandidate(candidate);
				map.put(pthItem, n);
			}
			else {
				NewNode n = map.get(pthItem);
				n.addCandidate(candidate);
			}
		}
	}
	
	public void addDataCase(DataCase match) {
//		System.out.println("Before--------------------------------------------------------");
//		for (DataCase dat : dataSequences)
//			System.out.println(dat);
//		System.out.println("--------------------------------------------------------------");
		if (!dataSequences.contains(match))
			dataSequences.add(match);
//		for (DataCase dat : dataSequences)
//			System.out.println(dat);
//		System.out.println("After---------------------------------------------------------");
	}
	
	public void update(int level) {
		if (depth == level) {
			for (DataCase match : dataSequences)
				sendAlong(match);
			//System.out.println("Removing obsolete datasequences...");
			dataSequences = null;
			List<EventAdapter> toBeRemoved = new ArrayList<>();
			for (Entry<EventAdapter,NewNode> entry : map.entrySet()) {
				NewNode node = entry.getValue();
				node.countSupport();
				if (node.candidates.values().stream().parallel().allMatch(set -> set.size() < Stats.MINIMUM_SUPPORT)) {
					toBeRemoved.add(entry.getKey());
				}
			}
			for (EventAdapter e : toBeRemoved)
				map.remove(e);
		}
		else if (level > depth)
			for (NewNode node : map.values())
				node.update(level);
	}
	
	//used for pruning also, frequent ones will have the necessary support in the candidates
	public void sendAlong(DataCase match) {
		if (map.size() != 0) {
			for (FrameAdapter frame : match.dataSequence.getSequence()) {
				if (frame.getTimestamp() >= match.timestamp - Stats.WINDOW_SIZE ) {
					if (frame.getTimestamp() <= match.timestamp + max(Stats.WINDOW_SIZE, Stats.MAX_GAP)) {
						//is within time gap
						for (EventAdapter event : frame.getEvents()) {
							if (!event.equals(match.hashedEvent)) {
//								System.out.println("" + event + " != " + match.hashedEvent);
								if (map.containsKey(event)) {
									NewNode n = map.get(event);
									n.addDataCase(new DataCase(event, match.dataSequence, frame.getTimestamp()));
								} 
							}
							else {
								//System.out.println("Disregarding because not equal");
								if (frame.getTimestamp() != match.timestamp) {
//									System.out.println("" + frame.getTimestamp() + " != " + match.timestamp);
									if (map.containsKey(event)) {
										NewNode n = map.get(event);
										n.addDataCase(new DataCase(event, match.dataSequence, frame.getTimestamp()));
									} 
								}
							}
//							if (!event.equals(match.hashedEvent) || frame.getTimestamp() != match.timestamp) {
//								if (map.containsKey(event)) {
//									NewNode n = map.get(event);
//									n.addDataCase(new DataCase(event, match.dataSequence, frame.getTimestamp()));
//								} 
								//if no candidate sequence generated an entry, then there is no reason to generate
								// one for data sequences 
						}
					}
					else //moved above, no reason to continue
						break;
				}
			}
		}
	}
	
	public void countSupport() {
		candidates.entrySet().parallelStream().forEach(entry -> {
			for (DataCase match : dataSequences) {
				if (!entry.getValue().contains(match.dataSequence)) {
					if (match.dataSequence.supports(entry.getKey())) {
						if (entry.getKey().size() > 1000000000) {
							System.out.println("Mother: " + match.dataSequence);
							System.out.println("Supports: " + entry.getKey());
						}
						entry.getValue().add(match.dataSequence);
					}
					else {
						if (entry.getKey().size() > 10000000) {
							System.out.println("Mother: " + match.dataSequence);
							System.out.println("Supports: " + entry.getKey());
						}
					}
				}
			}
		});
//		for (DataCase match : dataSequences) {
//			for (Entry<Sequence,Set<Sequence>> entry : candidates.entrySet()) {
//				if (!entry.getValue().contains(match.dataSequence)) {
//					if (match.dataSequence.supports(entry.getKey())) //isSupported(entry.getKey(), match.dataSequence)) 
//						entry.getValue().add(match.dataSequence);
//				}
//			}
//		}
	}
	
	public void addDataSequence(Sequence sequence) {
		if (depth == 1) { //interior node and root
			dataSequences.add(new DataCase(null,sequence,-1));
			for (FrameAdapter frame : sequence.getSequence()) {
				for (EventAdapter event : frame.getEvents()) {
					if (map.containsKey(event)) {
						NewNode n = map.get(event);
						DataCase match = new DataCase(event,sequence, frame.getTimestamp());
						n.addDataCase(match);
					}
				}
			}
		}
	}
	
	public List<Sequence> getSupportedOnLevel(int level) {
		if (level+1 == depth) {
			List<Sequence> results = new ArrayList<>();
			List<Sequence> toBeRemoved = new ArrayList<>();
			for (Entry<Sequence,Set<Sequence>> entry : candidates.entrySet()) {
				if (entry.getValue().size() >= Stats.MINIMUM_SUPPORT)
					results.add(entry.getKey());
				else
					toBeRemoved.add(entry.getKey());
				
			}
			for (Sequence seq : toBeRemoved)
				candidates.remove(seq);
			return results;
		}
		else if (level+1 > depth) {
			List<Sequence> results = new ArrayList<>();
			for (NewNode node : map.values())
			{
				List<Sequence> res = node.getSupportedOnLevel(level);
				if (res != null)
					results.addAll(res);
			}
			return results;
		}
		return null;
	}
	
	public int getSupport(Sequence candidate) {
		if (candidate.size() + 1 == depth) {
			if (candidates.containsKey(candidate)) {
				int support = candidates.get(candidate).size();
//				System.out.println("Candidate found, support: " + support);
				return support;
			}
			else {
				//System.out.println("Candidate not found. Returning -1...");
				return -1;
			}
		}
		else if (candidate.size() + 1 > depth) {
			EventAdapter pthItem = candidate.getPthItem(depth-1);
//			System.out.println("Trying to hash on item: " + pthItem);
			if (!map.containsKey(pthItem)) {
//				System.out.println("No path found, returning -1...");
				return -1;
			}
			else {
//				System.out.println("Path found, moving on...");
				return map.get(pthItem).getSupport(candidate);
			}
		}
		else {
//			System.out.println("Size: " + candidate.size() + " is too small");
			return -1;
		}
	}
	
	private static long max(long first, long second) {
		return first > second ? first : second;
	}
	
	private long getDataCaseCount() {
		long count = 0;
		for (NewNode node : map.values())
			count += node.getDataCaseCount();
//		System.out.println("----------------------------------------------------------------");
//		System.out.println("Data cases: ");
//		if (dataSequences != null)
//			for (DataCase data : dataSequences)
//				System.out.println("My hash = " + hashCode() + "Event: " + data.hashedEvent + " from seq: " + data.dataSequence.hashCode() + " with timestamp " + data.timestamp + " and with size " + data.dataSequence.size());
//		System.out.println("----------------------------------------------------------------");
		return dataSequences == null ? count : count + dataSequences.size();
	}
	
	public void printSize() {
//		System.out.println("Node at depth: " + depth + " has this many paths: " + map.size());
		long count = 0;
		for (NewNode node : map.values()) {
			count += node.getDataCaseCount();
			node.printSize();
		}
//		System.out.println("Number of data cases: " + count);
	}
}
