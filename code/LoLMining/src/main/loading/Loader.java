package main.loading;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.robrua.orianna.api.core.RiotAPI;
import com.robrua.orianna.api.core.StaticDataAPI;
import com.robrua.orianna.type.core.common.Region;
import com.robrua.orianna.type.core.staticdata.Champion;
import com.robrua.orianna.type.core.staticdata.Item;
import com.robrua.orianna.type.dto.match.MatchDetail;

public abstract class Loader {

	final static String apiKey = "4a01f280-e58e-4f53-87f9-325913a06cfd";
	private static final String SERIALIZED_CHAMPIONS_FILE = "champions.bin";

	private static ArrayList<MatchDetail> matchData = new ArrayList<>();
	private static HashMap<Long, String> championNameMapping = null;
	private static HashMap<Integer, String> itemNameMapping = null;
	private static HashMap<Long, Champion> championObjectMapping = null;
	private static HashMap<Integer, Item> itemObjectMapping = null;
	private static int numberDataFiles = 10;
	private static boolean hasRun = false;
	private static GsonBuilder gs = new GsonBuilder();

	public static void main(String[] arg) {

		Start();
	}

	public static void Start() {
		if (hasRun == true)
		{
			return;
		}
		RiotAPI.setAPIKey(apiKey);
		RiotAPI.setMirror(Region.EUNE);
		RiotAPI.setRegion(Region.EUNE);

		// "Do the Mapping"
		championObjectMapping = loadChampionsFromFileIfPossible();
		
		if (championObjectMapping == null){
			mapChamp();
			try {
				writeToFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		mapItem();

		try {
			loadFromFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		hasRun = true;
	}

	private static void mapItem() {
		if (itemNameMapping != null) {
			return;
		}

//		List<Item> itemstore = StaticDataAPI.getItems();
//		itemNameMapping = new HashMap<>();
//		itemstore.forEach(item -> {
//			itemNameMapping.put(item.getID(), item.getName());
//
//		});
	}

	private static void mapChamp() {
		if (championNameMapping != null) {
			return;
		}

		List<Champion> champstore = StaticDataAPI.getChampions();
		championNameMapping = new HashMap<>();
		championObjectMapping = new HashMap<Long, Champion>();
		champstore.forEach((champ) -> {
			championNameMapping.put(champ.getID(), champ.getName());
			championObjectMapping.put(champ.getID(), champ);
		});

	}

	private static void writeToFile() throws IOException {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(SERIALIZED_CHAMPIONS_FILE));
			out.writeObject(championObjectMapping);
			out.close();
		}
		catch (IOException e){
			System.out.println(e);
			e.printStackTrace();
		}
	}
	//will return null if failed to load
	private static HashMap<Long,Champion> loadChampionsFromFileIfPossible(){
		try {
			File file = new File(SERIALIZED_CHAMPIONS_FILE);
			if (file.exists()) {
				ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
				@SuppressWarnings("unchecked")
				HashMap<Long,Champion> data = (HashMap<Long,Champion>) in.readObject();
				in.close();
				return data;
			}
			return null;
		} catch(IOException | ClassNotFoundException e) {
			System.out.println(e);
			e.printStackTrace();
			return null;
		}
	}
	
	private static void loadFromFile() throws IOException {
		Gson gs = new Gson();
		String filename;
		for (int i = 1; i <= numberDataFiles; i++) {
			filename = "matches" + i + ".json";
			try (FileReader fileReader = new FileReader(filename);
					JsonReader jsReader = new JsonReader(fileReader);) {

				Matches goo = gs.fromJson(jsReader, Matches.class);
				jsReader.close();

				matchData.addAll(goo.matches);

			}
		}
	}

	public static List<Champion> mapChampions(List<Long> IDs) {
		ArrayList<Champion> champList = new ArrayList<>();
		IDs.forEach((ID) -> champList.add(championObjectMapping.get(ID)));
		return champList;
	}

	public static List<Item> mapItems(List<Long> IDs) {
		ArrayList<Item> itemList = new ArrayList<>();
		IDs.forEach(ID -> itemList.add(itemObjectMapping.get(ID)));
		return itemList;
	}

	public static List<MatchDetail> getMatches() {
		return matchData;
	}

	public static HashMap<Integer, String> getItemNameMapping() {
		return itemNameMapping;
	}

	public static HashMap<Long, String> getChampionNameMapping() {
		return championNameMapping;
	}

	public static Champion getChampion(long championID) {
		return championObjectMapping.get(championID);
	}

	public static Item getItem(int itemID) {
		return itemObjectMapping.get(itemID);
	}

	public static MatchDetail getMatchDetail(long matchID) {
		if (!matchData.isEmpty()) {
			for (MatchDetail match : matchData) {
				if (match.getMatchId() == matchID) {
					return match;
				}
			}
		}
		return null;
	}
}
