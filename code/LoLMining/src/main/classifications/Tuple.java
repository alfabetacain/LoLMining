package main.classifications;

import java.util.Map;

import com.robrua.orianna.type.dto.match.Team;

public class Tuple
{
	protected Team t;
	
	protected boolean firstBlood;
	protected boolean firstBaronKill;
	protected boolean firstDragonKill;
	protected boolean firstInhibitorDestroy;
	protected boolean firstTowerDestroy;
	protected boolean isWinner;
//	protected AmountOfKills dragonKills;
//	protected AmountOfKills inhibitorKills;
//	protected AmountOfKills towerKills;
//	protected AmountOfKills baronKills;
//	
	public Tuple(Team t, int meanDragonKills, int meanInhibitorKills, int meanTowerKills, int meanBaronKills)
	{
		this.t = t;
		isWinner = t.getWinner();
		firstBlood = t.getFirstBlood();
		firstBaronKill = t.getFirstBaron();
		firstDragonKill = t.getFirstDragon();
		firstInhibitorDestroy = t.getFirstInhibitor();
		firstTowerDestroy = t.getFirstTower();
//		dragonKills = loadDragonAmount(meanDragonKills);
//		inhibitorKills = loadInhibitorAmount(meanInhibitorKills);
//		towerKills = loadTowerAmount(meanTowerKills);
//		baronKills = loadBaronAmount(meanBaronKills);
	}
	
	
	
	private AmountOfKills loadBaronAmount(int mean)
	{
		if (t.getBaronKills() <= mean)
			return AmountOfKills.LESS_OR_EQUAL_TO_AVG;
		else if (t.getBaronKills() > mean)
			return AmountOfKills.GREATER_THAN_AVG;
		return null;
	}



	private AmountOfKills loadTowerAmount(int mean)
	{
		if (t.getTowerKills() <= mean)
			return AmountOfKills.LESS_OR_EQUAL_TO_AVG;
		else if (t.getTowerKills() > mean)
			return AmountOfKills.GREATER_THAN_AVG;
		return null;
	}



	private AmountOfKills loadInhibitorAmount(int mean)
	{
		if (t.getInhibitorKills() <= mean)
			return AmountOfKills.LESS_OR_EQUAL_TO_AVG;
		else if (t.getInhibitorKills() > mean)
			return AmountOfKills.GREATER_THAN_AVG;
		return null;
	}



	private AmountOfKills loadDragonAmount(int mean)
	{
		if (t.getDragonKills() <= mean)
			return AmountOfKills.LESS_OR_EQUAL_TO_AVG;
		else if (t.getDragonKills() > mean)
			return AmountOfKills.GREATER_THAN_AVG;
		return null;
	}
	
	



	public Object getAttribute(String s, Map<String,Integer> map)
	{
		if (s.equals("isWinner"))
		{
			return isWinner;
		}
		else if (s.equals("firstBlood"))
		{
			return firstBlood;
		}
		else if (s.equals("firstBaronKill"))
		{
			return firstBaronKill;
		}
		else if (s.equals("firstDragonKill"))
		{
			return firstDragonKill;
		}
		else if (s.equals("firstInhibitorDestroy"))
		{
			return firstInhibitorDestroy;
		}
		else if (s.equals("firstTowerDestroy"))
		{
			return firstTowerDestroy;
		}
		else if (s.equals("dragonKills"))
		{
			return loadDragonAmount(map.get(s));
		}
		else if (s.equals("inhibitorKills"))
		{
			return loadInhibitorAmount(map.get(s));
		}
		else if (s.equals("towerKills"))
		{
			return loadTowerAmount(map.get(s));
		}
		else if (s.equals("baronKills"))
		{
			return loadBaronAmount(map.get(s));
		}
		else
		{
			return null;
		}
	}
}

