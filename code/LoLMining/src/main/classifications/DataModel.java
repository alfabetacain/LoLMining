package main.classifications;

import java.util.LinkedList;
import java.util.List;

import com.robrua.orianna.type.dto.match.Team;

public class DataModel
{
	public static LinkedList<String> initializeTreeAttributes()
	{
		LinkedList<String> attributes = new LinkedList<String>();
		attributes.add("isWinner");
		attributes.add("firstBlood");
		attributes.add("firstBaronKill");
		attributes.add("firstDragonKill");
		attributes.add("firstInhibitorDestroy");
		attributes.add("firstTowerDestroy");
		attributes.add("dragonKills");
		attributes.add("inhibitorKills");
		attributes.add("towerKills");
		attributes.add("baronKills");
		return attributes;
	}
	
	public static LinkedList<String> removeAttribute(LinkedList<String> strings, String string)
	{
		for (int i = 0; i < strings.size(); i++)
		{
			if (string.equals(strings.get(i)))
			{
				strings.remove(i);
			}
		}
		return strings;
	}
	
	public static int getTypeLength(String a)
	{
		if (a.equals("isWinner") || a.equals("firstBlood") || a.equals("firstBlood") || a.equals("firstBaronKill") 
								 || a.equals("firstDragonKill") || a.equals("firstInhibitorDestroy")
								 || a.equals("firstTowerDestroy"))
		{
			return 2;
		}
		else if (a.equals("dragonKills") || a.equals("inhibitorKills") || a.equals("towerKills") || a.equals("baronKills"))
		{
			return AmountOfKills.values().length;
		}
		return 0;
	}
	
	public static int computeMean(List<Team> teams, String attribute)
	{
		int mean = 0;
		int total = 0;
		if (attribute.equalsIgnoreCase("DragonKills"))
		{
			for (Team t : teams)
			{
				mean += t.getDragonKills();
				total++;
				return mean / total;
			}
		}
		else if (attribute.equalsIgnoreCase("InhibitorKills"))
		{
			for (Team t : teams)
			{
				mean += t.getInhibitorKills();
				total++;
				return mean / total;
			}
		}
		else if (attribute.equalsIgnoreCase("TowerKills"))
		{
			for (Team t : teams)
			{
				mean += t.getTowerKills();
				total++;
				return mean / total;
			}
		}
		else if (attribute.equalsIgnoreCase("BaronKills"))
		{
			for (Team t : teams)
			{
				mean += t.getBaronKills();
				total++;
				return mean / total;
			}
			
		}
		return -1;
		
	}
}

enum AmountOfKills
{
	LESS_OR_EQUAL_TO_AVG,
	GREATER_THAN_AVG
}