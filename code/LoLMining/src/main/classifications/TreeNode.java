package main.classifications;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TreeNode implements Cloneable
{
	
	String label;
	String attLabel = null;
	TreeNode parent;
	TreeNode[] successors;
	List<Tuple> tuples;
	LinkedList<String> attributes;
	int[] classInfo;
	Integer mean = null;
	
	
	public TreeNode(String label, TreeNode parent, List<Tuple> tuples, LinkedList<String> attributes)
	{
		this.label = label;
		this.parent = parent;
		this.successors = null;
		this.tuples = tuples;
		this.attributes = attributes;
		
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
	
	public void print(String prefix, boolean isTail) {
        System.out.println(prefix + (isTail ? "!-- " : "|-- ") + label + "     ");
        for (int i = 0; i < successors.length - 1; i++) {
        	if (successors[i].successors == null)
        	{
        		successors[i].successors = new TreeNode[0];
        	}
        	successors[i].print(prefix + (isTail ? "    " : "|   "), false);
        }
        if (successors.length > 0) {
        	if (successors[successors.length - 1].successors == null)
        	{
        		successors[successors.length - 1].successors = new TreeNode[0];
        	}
        	successors[successors.length - 1].print(prefix + (isTail ?"    " : "|   "), true);
        }
        
    }
	
	public boolean isWinner(Tuple tuple) {
				
		if ((successors == null || successors.length == 0) && classInfo != null) {
			return classInfo[1] > classInfo[0] ? true : false;
		}
		else if (classInfo == null) {
			// that is because the Dj set is empty, then the class is assigned based on majority in D
			return parent.classInfo[1] > parent.classInfo[0] ? true : false;
		}
		
		String splitAttribute = attLabel;
		if (mean == null) {
			Boolean value = (Boolean) tuple.getAttribute(splitAttribute, null);
			if (value) 
				return successors[1].isWinner(tuple);
			else
				return successors[0].isWinner(tuple);
		}
		else {
			Map<String,Integer> map = new HashMap<>();
			map.put(splitAttribute, mean);
			Enum value = (Enum) tuple.getAttribute(splitAttribute, map);
			return successors[value.ordinal()].isWinner(tuple);
		}
	}
	
}