package main.classifications;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import main.loading.Loader;

import com.robrua.orianna.type.dto.match.MatchDetail;
import com.robrua.orianna.type.dto.match.Team;

public class Classifier
{
	protected LinkedList<String> attributes = DataModel.initializeTreeAttributes();
	protected  TreeNode root;
	
	public Classifier(List<Tuple> tuples, String targetAttribute) throws CloneNotSupportedException
	{
		attributes = DataModel.removeAttribute(attributes, targetAttribute);
		root = generate_decision_tree(null, tuples, targetAttribute, attributes, targetAttribute);
	}
	
	private TreeNode generate_decision_tree(TreeNode parent, List<Tuple> tuples, String targetAttribute, LinkedList<String> attributes, String label) throws CloneNotSupportedException
	{
		// Create node N (as book says on line 1)
		TreeNode node = new TreeNode(label, null, tuples, attributes);
		
		// Compute class entropy
		int[] classInfo = new int[DataModel.getTypeLength(targetAttribute)];
		int totalC = 0;
		for (int i = 0; i < node.tuples.size(); i++)
		{
			Object o = node.tuples.get(i).getAttribute(targetAttribute, null);
			
			// the case, if the target attribute (class) is Boolean
			if (o.getClass() == Boolean.class)
			{
				if ((boolean) o == false)
				{
					classInfo[0]++;
					totalC++;
				}
				else if ((boolean) o == true)
				{
					classInfo[1]++;
					totalC++;
				}
			}
		}
		double classEntropy = computeClassEntropy(classInfo, totalC);
		
		node.classInfo = classInfo;
		
		//
		// All the logic assumes that a target attribute is Boolean, then
		// classEntropy[0] = false value
		// classEntropy[1] = true value
		//
		
		// if all tuples in D are all of the same class (as book says on line 2)
		if (classEntropy == 0.0)
		{
			String classValue = "";
			int i = -1;
			if (classInfo[0] <= classInfo[1])
			{
				classValue = "true";
				i = 1;
			}
				
			else
			{
				classValue = "false";
				i = 0;
			}
				
			
			// return this stuff as leaf  (as book says on line 3)
			node.label += " - Leaf node (classified) " + "Label: " + targetAttribute + " = " + classValue + " " + "Size = "+ node.tuples.size() + " Outcome purity = "+ classInfo[i];
			return node;
		}
		
		// if attribute list is empty  (as book says on line 4)
		if (node.attributes.size() < 1)
		{
			String classValue = "";
			int i = -1;
			if (classInfo[0] <= classInfo[1])
			{
				classValue = "true";
				i = 1;
			}
				
			else
			{
				classValue = "false";
				i = 0;
			}
			
			// return this stuff applying majority voting  (as book says on line 5)
			node.label += " - Leaf node (no attrs) "  + "Label: " + targetAttribute + " = " + classValue + " " + "Size = "+ node.tuples.size() + " Outcome purity = "+ classInfo[i];
			return node;
		}
		
		//calculate mean
		int dragonValue, baronValue, inhibitorValue,towerValue;
		dragonValue = baronValue = inhibitorValue = towerValue = 0;
		for (int i = 0; i < tuples.size(); i++) {
			Tuple current = tuples.get(i);
			dragonValue += current.t.getDragonKills();
			baronValue += current.t.getBaronKills();
			inhibitorValue += current.t.getInhibitorKills();
			towerValue += current.t.getTowerKills();
		}
		Map<String, Integer> map = new HashMap<>();
		map.put("dragonKills", dragonValue / tuples.size());
		map.put("inhibitorKills", inhibitorValue / tuples.size());
		map.put("towerKills", towerValue / tuples.size());
		map.put("baronKills", baronValue / tuples.size());
		
		
		// splitting attribute selection  (as book says on line 6)
		double maxGain = -1;
		String attributeLabel = "";
		for (int i = 0; i < node.attributes.size(); i++)
		{
			// attribute properties
			String a = node.attributes.get(i);
			int[][] attributeGainInfo = new int[DataModel.getTypeLength(a)][DataModel.getTypeLength(targetAttribute)];
			int[] attributeSplitInfo = new int[DataModel.getTypeLength(a)];
			int totalA = 0;
			// attribute discovery
			for (int j = 0; j < node.tuples.size(); j++)
			{
				Object o1 = node.tuples.get(j).getAttribute(a, map);
				Object o2 = node.tuples.get(j).getAttribute(targetAttribute, map);
				
				if (o1 != null && o2 != null)
				{
					// the case, if splitting attribute is Boolean and target attribute is Boolean
					if (o1.getClass() == Boolean.class && o2.getClass() == Boolean.class)
						if ((boolean) o1 == false && (boolean) o2 == false)
						{
							attributeGainInfo[0][0]++;
							attributeSplitInfo[0]++;
							totalA++;
						}
						else if ((boolean) o1 == true && (boolean) o2 == false)
						{
							attributeGainInfo[1][0]++;
							attributeSplitInfo[1]++;
							totalA++;
						}
						else if ((boolean) o1 == false && (boolean) o2 == true)
						{
							attributeGainInfo[0][1]++;
							attributeSplitInfo[0]++;
							totalA++;
						}
						else
						{
							attributeGainInfo[1][1]++;
							attributeSplitInfo[1]++;
							totalA++;
						}
					// the case, if splitting attribute is Enum and target attribute is Boolean
					else if (o1.getClass().isEnum() == true && o2.getClass() == Boolean.class)
					{
						if ((boolean) o2 == false)
						{
							attributeGainInfo[((Enum) o1).ordinal()][0]++;
							attributeSplitInfo[((Enum) o1).ordinal()]++;
							totalA++;
						}
						else if ((boolean) o2 == true)
						{
							attributeGainInfo[((Enum) o1).ordinal()][1]++;
							attributeSplitInfo[((Enum) o1).ordinal()]++;
							totalA++;
						}
					}
				}
				
			}
			// attribute selection measures
			if (attributeSplitInfo.length == 2)
			{
				// if restricted to binary trees (as book says on line 8)
				double attributeEntropy = computeAttributeEntropy(attributeGainInfo, totalA);
				double gain = classEntropy - attributeEntropy;
				// best next splitting attribute
				if (gain > maxGain)
				{
					maxGain = gain;
					// this label would be assigned (as book says on line 7)
					attributeLabel = a;
				}
			}
			else
			{
				// if not restricted to binary trees (as book says on line 8)
				double attributeEntropy = computeAttributeEntropy(attributeGainInfo, totalA);
				double gain = classEntropy - attributeEntropy;
				double splitInfo = computeClassEntropy(attributeSplitInfo, totalA);
				double gainRatio = gain / splitInfo;
				// best next splitting attribute
				if (gainRatio > maxGain)
				{
					maxGain = gainRatio;
					// this label would be assigned (as book says on line 7)
					attributeLabel = a;
				}
			}
		}
		// remove splitting attribute (as book says on line 9)
		node.attributes = DataModel.removeAttribute(node.attributes, attributeLabel);
		
		if (attributeLabel.equals("") == false)
		{
			// partition the tuples
			ArrayList<Tuple>[] disterbutedInstances = new ArrayList[DataModel.getTypeLength(attributeLabel)];
			for (int i = 0; i < disterbutedInstances.length; i++)
			{
				disterbutedInstances[i] = new ArrayList<Tuple>();
			}
			
			// for each outcome j (i) of splitting criterion (as book says on line 10 (partition part))
			for (int i = 0; i < node.tuples.size(); i++)
			{
				Object o = node.tuples.get(i).getAttribute(attributeLabel, map);
				// the case, if splitting attribute is Boolean
				if (o != null && o.getClass() == Boolean.class)
				{
					if ((boolean) o == false)
						disterbutedInstances[0].add(node.tuples.get(i));
					else if ((boolean) o == true)
						disterbutedInstances[1].add(node.tuples.get(i));
				}
				// the case, if splitting attribute is Enum
				else if (o != null && o.getClass().isEnum() == true)
				{
						disterbutedInstances[((Enum) o).ordinal()].add(node.tuples.get(i));
				}
			}
			node.successors = new TreeNode[disterbutedInstances.length];
			
			//if mean was used, save it in the node
			if (tuples.get(0).getAttribute(attributeLabel, map).getClass().isEnum()) {
				node.mean = map.get(attributeLabel);
			}
			
			// grow subtrees for each partition (as book says on line 10 (grow part))
			// disterbutedInstances[j] (Dj) is the set of data tuples in D satisfying outcome  (as book says on line 11)
			for(int j = 0; j < node.successors.length; j++)
			{
				LinkedList<String> newAttributes = new LinkedList<String>(node.attributes);
				
				// if Dj is empty, then (book line 12)
				if (disterbutedInstances[j].size() == 0)
				{
					String classValue = "";
					if (classInfo[0] <= classInfo[1])
						classValue = "true";
					else
						classValue = "false";
					
					// attach a leaf labeled with the majority class in D to node N (book line 13)
					String iLabel = "Leaf node (Dj is empty node) "  + "Label (based on majority voting in D): " + targetAttribute + " = " + classValue;
					node.successors[j] = new TreeNode(iLabel, node, null, newAttributes);
				}
				else
				{
					// attach the node returned by generate_decision_tree (book line 14)
					String iLabel = attributeLabel; 
					node.attLabel = iLabel;
					node.successors[j] = generate_decision_tree(node, disterbutedInstances[j], targetAttribute, newAttributes, iLabel);
					
				}
			}
		}
		// return N (book line 15)
		return node;  // tadaaa :)
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static double computeClassEntropy(int[] classInfo, int total)
	{
		double entropy = 0;
		for (int i = 0; i < classInfo.length; i++)
		{
			if (classInfo[i] > 0)
			{
				entropy += -(double) classInfo[i] / (double) total * 
						Math.log((double) classInfo[i] / (double) total) / Math.log(2);
			}
			
		}
		return entropy;
	}
	
	public static double computeAttributeEntropy(int[][] attributeInfo, int total)
	{
		double entropy = 0;
		for (int i = 0; i < attributeInfo.length; i++)
		{
			int rowTotal = 0;
			for (int j = 0; j < attributeInfo[i].length; j++)
			{
				rowTotal += attributeInfo[i][j];
			}
			entropy += ((double) rowTotal / (double) total) * (computeClassEntropy(attributeInfo[i], rowTotal));
		}
		return entropy;
	}
	
	public void print()
	{
		root.print("", true);
	}
	
	public static void main(String[] args) throws CloneNotSupportedException
	{
		/*
		RiotAPI.setRegion(Region.NA);
		RiotAPI.setMirror(Region.NA);
		RiotAPI.setAPIKey("0b64a0a6-9273-4818-a376-fbcf09955f76");
		/*
		Match m1 = MatchAPI.getMatch(1694556929);
		Match m2 = MatchAPI.getMatch(1694556931);
		Match m3 = MatchAPI.getMatch(1694556934);
		Match m4 = MatchAPI.getMatch(1694556937);
		*/
		
		Loader.Start();
		// System.out.println(Loader.getMatches().size());
		
		List<MatchDetail> matches = Loader.getMatches();
		
		List<Team> teams = new ArrayList<Team>();

		for (MatchDetail md : matches)
		{
			teams.addAll(md.getTeams());
		}
		
		ArrayList<Tuple> tuples = new ArrayList<Tuple>();
		
		int meanDragonKills = DataModel.computeMean(teams, "DragonKills");
		int meanInhibitorKills = DataModel.computeMean(teams, "InhibitorKills");
		int meanTowerKills = DataModel.computeMean(teams, "TowerKills");
		int meanBaronKills = DataModel.computeMean(teams, "BaronKills");
		/*
		System.out.println(meanDragonKills);
		System.out.println(meanInhibitorKills);
		System.out.println(meanTowerKills);
		System.out.println(meanBaronKills);
		
		*/
		
		for (Team t : teams)
		{
			tuples.add(new Tuple(t, meanDragonKills, meanInhibitorKills, meanTowerKills, meanBaronKills));
		}
		
		List<Tuple> forClassifier = tuples.subList(0, 1500);
		List<Tuple> forTesting = tuples.subList(1500, 2000);
		
		Classifier c = new Classifier(forClassifier, "isWinner");
		//c.start(tuples, "isWinner");
		c.print();
		
		double truePos,falsePos,trueNeg,falseNeg;
		truePos = falsePos = trueNeg = falseNeg = 0.0;
		double allPositive = 0;
		double allNegative = 0;
		for (Tuple tuple : forTesting) {
			boolean result = c.root.isWinner(tuple);
			if (tuple.isWinner)
				allPositive += 1.0;
			else
				allNegative += 1.0;
			
			
			if (tuple.isWinner && result) 
				truePos += 1.0;
			else if (tuple.isWinner && !result) 
				falseNeg += 1.0;
			else if (!tuple.isWinner && result) 
				falsePos += 1.0;
			else
				trueNeg += 1.0;
		}
		
		//test results
		double accuracy = (truePos + trueNeg) / (allPositive + allNegative);
		System.out.println("Accuracy: " + accuracy);
		double errorRate = (falsePos + falseNeg) / (allPositive + allNegative);
		System.out.println("Error rate: " + errorRate);
		double sensitivity = truePos / allPositive;
		System.out.println("Sensitivity: " + sensitivity);
		double specificity = trueNeg / allNegative;
		System.out.println("Specificity: " + specificity);
		double precision = truePos / (truePos + falsePos);
		System.out.println("Precision: " + precision);
		double harmonic = (2 * precision * sensitivity) / (precision + sensitivity);
		System.out.println("Harmonic: " + harmonic);
		
	}
	
}
