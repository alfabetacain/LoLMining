import main.apriori.Aprior;
import main.apriori.NewNode;
import main.loading.Loader;


public class Runner {

	public static void main(String[] args) {
		Loader.Start();
		Aprior apriori = new Aprior();
		//loader.getMatches().stream().forEach(m -> m.getTimeline().getFrames().stream().forEach(f -> System.out.println(f)));
		NewNode node = apriori.patternify(Loader.getMatches());
	}
}
